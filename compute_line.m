function l = compute_line(values, idx)
  p1 = [A(idx,1) A(idx,2) 1]';
  p2 = [A(idx,3) A(idx,4) 1]';
  
  l = p2 - p1;
endfunction
